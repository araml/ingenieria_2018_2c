!classDefinition: #QueueTest category: #'Queue-Exercise'!
TestCase subclass: #QueueTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test01QueueShouldBeEmptyWhenCreated

	| queue |

	queue _ Queue new.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test02EnqueueAddElementsToTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.

	self deny: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test03DequeueRemovesElementsFromTheQueue

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test04DequeueReturnsFirstEnqueuedObject

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'Something1'.
	secondQueued _ 'Something2'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.
	
	self assert: queue dequeue equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'CGCM 9/27/2018 16:34:58'!
test05QueueBehavesFIFO

	| queue firstQueued secondQueued |

	queue _ Queue new.
	firstQueued _ 'First'.
	secondQueued _ 'Second'.
	queue enqueue: firstQueued.
	queue enqueue: secondQueued.

	self assert: queue dequeue equals: firstQueued.
	self assert: queue dequeue equals: secondQueued.
	self assert: queue isEmpty.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test06NextReturnsFirstEnqueuedObject

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	self assert: queue next equals: firstQueued.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test07NextDoesNotRemoveObjectFromQueue

	| queue firstQueued |

	queue _ Queue new.
	firstQueued _ 'Something'.
	queue enqueue: firstQueued.

	queue next.
	self assert: queue size equals: 1.! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test08CanNotDequeueWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.
	
	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test09CanNotDequeueWhenThereAreNoObjectsInTheQueueAndTheQueueHadObjects

	| queue |

	queue _ Queue new.
	queue enqueue: 'Something'.
	queue dequeue.

	self
		should: [ queue dequeue ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !

!QueueTest methodsFor: 'test' stamp: 'HAW 9/26/2018 23:15:51'!
test10CanNotNextWhenThereAreNoObjectsInTheQueue

	| queue |

	queue _ Queue new.

	self
		should: [ queue next ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: Queue queueEmptyErrorDescription ].! !


!classDefinition: #Queue category: #'Queue-Exercise'!
Object subclass: #Queue
	instanceVariableNames: 'first'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:18:46'!
dequeue
	| temp_data |
	temp_data _ first.
	first _ first dequeue.
	^ temp_data contents.! !

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:09:11'!
enqueue: anElement
	first _ (first enqueue: (QueueElement with: anElement)).! !

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 01:58:55'!
initialize
	first _ QueueEmpty new.! !

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 01:59:22'!
isEmpty
	^ first size = 0.! !

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:22:19'!
next
	^ first contents.! !

!Queue methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:23:00'!
size
	^ first size.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Queue class' category: #'Queue-Exercise'!
Queue class
	instanceVariableNames: ''!

!Queue class methodsFor: 'error descriptions'!
queueEmptyErrorDescription
	^ 'Queue is empty'.! !


!classDefinition: #QueueEnd category: #'Queue-Exercise'!
Object subclass: #QueueEnd
	instanceVariableNames: 'content next'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueEnd methodsFor: 'as yet unclassified' stamp: 'LR 10/3/2018 22:40:14'!
dequeue
	self subclassResponsibility ! !

!QueueEnd methodsFor: 'as yet unclassified' stamp: 'LR 10/3/2018 22:40:20'!
enqueue: anElement
	self subclassResponsibility ! !

!QueueEnd methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:02:53'!
next: anElement
	self subclassResponsibility ! !

!QueueEnd methodsFor: 'as yet unclassified' stamp: 'LR 10/3/2018 22:40:25'!
size
	self subclassResponsibility ! !


!classDefinition: #QueueElement category: #'Queue-Exercise'!
QueueEnd subclass: #QueueElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:12:21'!
contents
	^ content.! !

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:10:00'!
dequeue
	^ next.! !

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:21:37'!
enqueue: anElement
	next _ (next enqueue: anElement).
	^ self.! !

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:06:16'!
initializeWith: data
	content _ data.! !

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:03:19'!
next: anElement
	next _ anElement .! !

!QueueElement methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 01:50:40'!
size
	^ 1 + next size.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'QueueElement class' category: #'Queue-Exercise'!
QueueElement class
	instanceVariableNames: ''!

!QueueElement class methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:05:42'!
with: content
	^ QueueElement new initializeWith: content.! !


!classDefinition: #QueueEmpty category: #'Queue-Exercise'!
QueueEnd subclass: #QueueEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Queue-Exercise'!

!QueueEmpty methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:31:02'!
contents
	^ self dequeue.! !

!QueueEmpty methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:50:44'!
dequeue
	self error: Queue queueEmptyErrorDescription.! !

!QueueEmpty methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 02:01:12'!
enqueue: anElement
	anElement next: self.
	^ anElement.! !

!QueueEmpty methodsFor: 'as yet unclassified' stamp: 'LR 10/4/2018 01:49:28'!
size
	^ 0.! !
